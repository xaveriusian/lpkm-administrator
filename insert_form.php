<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Administrator - Insert Data</title>
</head>
<body>
    <h2>Insert Data</h2>
    <form method="post" action="insert_data.php" enctype="multipart/form-data">
        <label for="no">No:</label>
        <input type="text" id="no" name="no"><br>
        <label for="tahun">Tahun:</label>
        <input type="text" id="tahun" name="tahun"><br>
        <label for="judul">Judul Kegiatan:</label>
        <input type="text" id="judul" name="judul"><br>
        <label for="summary">Summary:</label>
        <input type="text" id="summary" name="summary"><br>
        <label for="file">Import Data from Excel:</label>
        <input type="file" id="file" name="file"><br>
        <input type="submit" value="Submit">
    </form>
</body>
</html>
