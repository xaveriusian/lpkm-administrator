<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>View Data</title>
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
<style>
    /* Style the accordion */
    .accordion {
        background-color: #eee;
        color: #444;
        cursor: pointer;
        padding: 18px;
        width: 100%;
        text-align: left;
        border: none;
        outline: none;
        transition: 0.4s;
    }
    .active, .accordion:hover {
        background-color: #ccc;
    }
    .panel {
        padding: 0 18px;
        background-color: white;
        display: none;
        overflow: hidden;
    }
          body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
        }
        h2 {
            text-align: center;
            margin-top: 20px;
        }
        table {
            width: 80%;
            margin: 20px auto;
            border-collapse: collapse;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
            background-color: #fff;
        }
        th, td {
            padding: 12px 15px;
            border-bottom: 1px solid #ddd;
            text-align: center;
        }
        th.no, td.no,
        th.year, td.year {
            text-align: center;
        }
        tr:hover {
            background-color: #f2f2f2;
        }
        th {
            background-color: #4CAF50;
            color: white;
        }
</style>
</head>
<body>

<button class="accordion">Data Display</button>
<div class="panel">
    <table id="dataTable">
        <thead>
            <tr>
                <th>No</th>
                <th>Tahun</th>
                <th>Judul Kegiatan</th>
                <th>Summary</th>
            </tr>
        </thead>
        <tbody>
            <?php
            // Fetch data from the database and populate the table
            $servername = "localhost";
            $username = "jacobwij_jacob";
            $password = "sabakuseptian553";
            $dbname = "jacobwij_lpkm-administrator";

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);

            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM kegiatan";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    echo "<tr><td>" . $row["no"] . "</td><td>" . $row["tahun"] . "</td><td>" . $row["judul_kegiatan"] . "</td><td>" . $row["summary"] . "</td></tr>";
                }
            }
            $conn->close();
            ?>
        </tbody>
    </table>
</div>


<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        // Initialize DataTable
        $('#dataTable').DataTable();

        // Accordion functionality
        $('.accordion').click(function() {
            $(this).toggleClass('active');
            $(this).next('.panel').slideToggle();
        });
    });

</script>
</body>
</html>
